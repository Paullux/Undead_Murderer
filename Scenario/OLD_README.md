# Undead murderer

## Notes

### Menu

Choix nombre joueurs :

    • si 1 on lance le jeux,

    • si plus, on cherche des serveurs pour le jeux avec nombre de joueurs inscrit dans la partie, max 4 joueurs.

Choix avatar :  quatre avatars hommes et quatre avatars femmes pour les joueurs.
Modes un joueur/multijoueurs identiques.

### Idée dévoilée à la fin

Les joueurs entendent des voix et voit les humains comme des zombies exceptés les autres joueurs, progressivement la mort commence par un cauchemar (plein de morts partout) puis progressivement en cas de décès, fin du jeu approchant, vers la fin le joueur se retrouve en asile (par exemple d’abord attaché à un lit, puis vers la fin attaché et avec un psychiatre qui lui parle). 
Le mode multi joueur est coopératif.

L’humain mort en jeu devient zombie.

S’il recharge il retourne en jeux avec les autres joueurs.

A la fin du jeux les joueurs sont arrêtés séparément par des infirmiers et mis dans un camion. Effet sommeil, et le joueur est seul attaché à un lit.
Un psychiatre lui raconte :

    • Qu’il est un fou furieux qu’il a fait du mal à plein de gens.

    • Si multijoueur, le joueur lui demande mais où sont mes amis, le psy lui répond qu’il n’a jamais eu d’amis et qu’il est seul.

#### Premier épisode

Le(s) joueur(s) est(sont) seul(s) sur une île, leur lieu de vie.

**Cinématique** : Effet caméra, du décor (île entière) vers les avatars joueurs. Le joueur 1 allume une télé, là une journaliste en plateau décrit qu’une étrange maladie contamine très rapidement tout le monde, la voix journaliste est issue d’un synthèse vocale, miniature vidéo ville avec des zombies. L’avatar 1 coupe la télé et dit «qu’est-ce que c’est cette connerie ?».

**Discours journaliste** : Une maladie inconnue et très contagieuse est entrain de contaminer une grande partie de la population. Les scientifiques ignorent encore quel est le germe à l’origine de cette affection. Ils travaillent afin de comprendre la maladie et ses causes sans résultat pour l’instant. Les gens infectés se déplacent de manière étrange et cherchent à contaminer des personnes encore saines.

Le jeu commence, une instruction vocale (la voix intérieur du joueur, puis expression d’un des joueurs), dit on doit retourner vers le continent pour voir ça.

Là les joueurs doivent aller à l‘embarcadère pour prendre un bateau.

#### Second épisode

Les joueurs doivent aller sur le continent.

#### Les courses mensuelles

Le joueur commence à entrer au port. Il peut commencer à se déplacer et son but est d'acheter des vivres pour le mois. Il doit alors passer par plusieurs maisons pour atteindre un centre commercial. Après quelques pas, un événement.

#### Cinématique : mauvaise rencontre

Lorsque le héro est arrivé dans le village, il fait noir. On voit à peine plus de 10 mètres car un brouillard est présent. Le héro entend des cris et s'en approche. Les cris viennent d'une maison. Au bas de la porte, le cri s'est tu, laissant seulement le bruit de la chaire du défunt prendre le relai. Notre héro se rend compte de l'horreur en voyant ces trois prédateurs en plein repas. Tout à coup, ils le regardent !

#### Course folle

Le joueur ne peut pas se défendre, il doit fuir. S'il tente de rejoindre la mer, il découvre des zombis lui barre la route. S'il fonce vers le centre commercial, il a une chance de survivre. Coup de bol, une porte du centre commercial est ouverte. Le héro bloque alors la porte de l'intérieur. Par contre, il fait noir à l'intérieur. 
