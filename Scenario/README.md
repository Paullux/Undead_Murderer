# Undead Murderer

## Notes

### Menu

Choix nombre joueurs :

* Si 1 on lance le jeux,

* Si plus, on cherche des serveurs pour le jeux avec nombre de joueurs inscrit dans la partie, max 4 joueurs.

Choix avatar :  quatre avatars hommes et quatre avatars femmes pour les joueurs.
Modes un joueur/multijoueurs identiques.

### Idée dévoilée à la fin

Les joueurs entendent des voix et voit les humains comme des zombies exceptés les autres joueurs, progressivement la mort commence par un cauchemar (plein de morts partout) puis progressivement en cas de décès, fin du jeu approchant, vers la fin le joueur se retrouve en asile (par exemple d’abord attaché à un lit, puis vers la fin attaché et avec un psychiatre qui lui parle). 
Le mode multi joueur est coopératif.

L’humain mort en jeu devient zombie.

S’il recharge il retourne en jeux avec les autres joueurs.

A la fin du jeux les joueurs sont arrêtés séparément par des infirmiers et mis dans un camion. Effet sommeil, et le joueur est seul attaché à un lit.
Un psychiatre lui raconte :

* Qu’il est un fou furieux qu’il a fait du mal à plein de gens.

* Si multijoueur, le joueur lui demande mais où sont mes amis, le psy lui répond qu’il n’a jamais eu d’amis et qu’il est seul.

### Note du premier épisode

Le(s) joueur(s) est(sont) seul(s) sur une île, leur lieu de vie.

**Cinématique** : Effet caméra, du décor (île entière) vers les avatars joueurs. Le joueur 1 allume une télé, là une journaliste en plateau décrit qu’une étrange maladie contamine très rapidement tout le monde, la voix journaliste est issue d’un synthèse vocale, miniature vidéo ville avec des zombies. L’avatar 1 coupe la télé et dit «qu’est-ce que c’est cette connerie ?».

**Discours journaliste** : Une maladie inconnue et très contagieuse est entrain de contaminer une grande partie de la population. Les scientifiques ignorent encore quel est le germe à l’origine de cette affection. Ils travaillent afin de comprendre la maladie et ses causes sans résultat pour l’instant. Les gens infectés se déplacent de manière étrange et cherchent à contaminer des personnes encore saines.

Le jeu commence, une instruction vocale (la voix intérieur du joueur, puis expression d’un des joueurs), dit on doit retourner vers le continent pour voir ça.

Là les joueurs doivent aller à l‘embarcadère pour prendre un bateau.

### Personnages

* Individus croisés de plus en plus nombreux.
* Il a des « flash Réalité » de gens normaux / de zombies
* Plus on tue de zombies, plus on est vite arrêté et plus le jeux finit tôt
* **Soin vie** : restaure la vie mais pas la contamination, du coup le joueur continu à perdre de la vie
* **Soin décontamination** : guérit la contamination mais ne restaure pas la vie

## Histoire

*Histoire suggérée par Alnotz.*

### Épisode 1 : actualité & ventre vide

Comme tout les soirs, sur son île paisible, le même rituel. Dès le sommet du phare allumé, une petite sieste sur le canapé à grignoter les informations du monde. En allumant sont téléviseur il découvre quelques nouvelles originales sur une épidémie semblant se propager rapidement dans le Globe. Il se rend aussi compte que sont réfrigérateur est vide.

Face à l'appel incessant de la faim, notre gardien aura du mal à échapper à corvée des vivres du mois. Tant pis, il fera ses courses tard le soir. À l'extérieur du phare, le Soleil est déjà couché, laissant place au vent glacé du Nord. Une fois installé sur sa barque, il partira en direction du port le plus proche.

### Épisode 2 : Les courses mensuelles

La route est longue avant d'atteindre la ville. L'hiver arrivé, des calottes de glaces apparaissent. Difficile de naviguer face à de tels obstacles. Finalement, le port et ses navires apparaissent.

Il commence par amarrer sa barque avant de commencer le début des courses. On n'entend alors que le murmure de la mer. Il sort du port et se dirige vers le centre commercial. Pourvu que ce soit encore ouvert. 

### Épisode 3 : mauvaise rencontre

Lorsque notre gardien est arrivé dans le village, il voit à peine plus de 10 mètres car un brouillard est présent.

**Cinématique** : Il entend des cris et s'en approche. Les cris viennent d'une maison. Au bas de la porte, le cri s'est tu, laissant seulement le bruit de la chaire du défunt prendre le relais. Il se rend compte de l'horreur en voyant ce prédateur en plein repas. Tout à coup, la créature le regarde !

Est-ce un homme ?! s'interrogea notre antagoniste. Ne pouvant pas se défendre, il doit fuir ! S'il tente de rejoindre la mer, il découvre des zombies lui barrant la route. S'il fonce vers le centre commercial, il a une chance de survivre.

Mais pourquoi y a-t-il des barricades bloquant des rues ? Il doit dépêcher car les cris glaçant sont plus proches, plus nombreuses. Il trouve enfin le centre commercial ! Par contre l'entrée principale est fermée ! Il doit trouver une autre entré. Par chance, une porte arrière est entre-ouverte, la serrure brisée.

Par précaution, il récupère non loin un balai et bloque la porte de l'intérieur. Par contre, il fait noir à l'intérieur.

### Épisode 4 : Embarras du choix

Tant qu'à faire, autant prendre des vivres maintenant. Notre gardien explore alors le bâtiment et ses nombreux magasins. La majorité est fermée. Certaines vitres sont brisées. Il hésite un peu avant de s'engouffrer dans une des boutiques par une forte défoncée. Heureusement, rien de tel que son téléphone pour dissiper un peu les ténèbres. Cette première boutique est une confiserie d'après ces pommes d'amour est autres sucreries à travers les vitrines brisées. Certains se seraient déjà servis ? C'est tentant.

En explorant le centre commercial, il tombe aussi sur une boucherie artisanale. Ça fait longtemps qu'il n'a pas profité d'un vrai pot-au-feu avec de la moelle à tartiner. Bizarre, il y a encore la clef accrochée à la serrure. Le boucher est encore là ? La chambre froide est aussi ouverte. Mmm... Bœuf, porc, mouton, poulet, femme... Quoi !? Il rendit compte qu'une femme morte était aussi suspendue par un crocher dans la pénombre. Mieux vaut déguerpir et vite.

Dans le centre commercial, il a aussi cru voir une silhouette gémir au font d'un couloir. Il décide alors de rebrousser chemin !

### Épisode 5 : Pas si furtif

**Cinématique** : Il a jugé bon de retourner vers la porte arrière du centre commercial mais c'était avec la certitude... qu'elle serait encore fermée. Or elle est bien ouverte ! Quelqu'un derrière lui gît sur le sol avant de se relever. Merde, cette horreur l'a retrouvé et semble bien parti pour lui sauter dessus ! Heureusement, l'horreur a pu être esquivée de justesse.

Il va falloir se planquer quelque part pour lui fausser compagnie. Heureusement, il y a un magasin de bricolage qui n'a pas été fermé entièrement. Il se faufile dedans mais se fait prendre par un pied ! Il insiste alors de tout ses forces et finit par passer... au pris d'une chaussure. Alors qu'il se croyait enfin tranquille, son poursuivant tente de défoncer la porte. Pas le temps de trop penser, il récupère une pelle à porté de main. Incroyable : la porte commence à céder !

Il devra choisir : attendre que la chose se lasse de chercher ou tenter de s'en débarrasser définitivement. Ça y est, il approche !

**Choix 1 : confrontation** : c'est décidé, il est temps de le défoncer ! On attend le bon moment et un bon coup de pelle dans la gueule ! On n'hésite pas à pulvériser la tête. Encore et encore ! Après avoir traversé le crâne pas un coup de grâce, le monstre ne devrait plus se relever. Heu... par contre, la pelle est restée bien enfoncée dans le crâne.

**Choix 2 : fuite réussie** : c'est décidé, on va enfermer le zombie dans une salle. Il fonce alors vers une salle annexe, poursuivi pas la chose. Cette dernière est quand-même rapide ! Il fait le tour des rayons et finit par ressortir de l'annexe en bloquant la port avec sa pelle. Notons que cette porte est renforcée. Ça devrait tenir un moment.

Ressorti dehors, il reprend enfin son souffle.

### Épisode 6 : La bonne affaire
